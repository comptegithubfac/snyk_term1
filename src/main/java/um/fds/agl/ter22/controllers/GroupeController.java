package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.forms.GroupeForm;
import um.fds.agl.ter22.services.GroupeService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class GroupeController implements ErrorController {

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private GroupeService groupeService;

    @GetMapping("/listGroupes")
    public Iterable<Groupe> getGroupes(Model model) {
        model.addAttribute("groupes", groupeService.getGroupes());
        return groupeService.getGroupes();
    }
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = { "/addGroupe" })
    public String showAddPersonPage(Model model) {

        GroupeForm groupeForm = new GroupeForm();
        model.addAttribute("groupeForm", groupeForm);

        return "addGroupe";
    }

    @PostMapping(value = { "/addGroupe"})
    public String addGroupe(Model model, @ModelAttribute("GroupeForm") GroupeForm groupeForm) {
        Groupe groupe;
        if(groupeService.findById(groupeForm.getId()).isPresent()){
            groupe = groupeService.findById(groupeForm.getId()).get();
            groupe.setTitle(groupeForm.getTitle());
        } else {
            groupe = new Groupe(groupeForm.getTitle());
        }
        groupeService.saveGroupe(groupe);
        return "redirect:/listGroupes";

    }

    @GetMapping(value = {"/showGroupeUpdateForm/{id}"})
    public String showGroupeUpdateForm(Model model, @PathVariable(value = "id") long id){

        GroupeForm groupeForm = new GroupeForm(id, groupeService.findById(id).get().getTitle());
        model.addAttribute("groupeForm", groupeForm);
        return "updateGroupe";
    }

    @GetMapping(value = {"/deleteGroupe/{id}"})
    public String deleteGroupe(Model model, @PathVariable(value = "id") long id){
        groupeService.deleteGroupe(id);
        return "redirect:/listGroupes";
    }
}

