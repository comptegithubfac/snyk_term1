package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Assignment;

public interface AssignmentRepository<T extends Assignment> extends CrudRepository<T, Long> {

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    Assignment save(@Param("assignment") Assignment assignment);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("assignment") Assignment assignment);
}
