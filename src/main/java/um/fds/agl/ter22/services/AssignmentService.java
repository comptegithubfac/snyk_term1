package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Assignment;
import um.fds.agl.ter22.repositories.AssignmentRepository;


import java.util.Optional;


@Service
public class AssignmentService {

    @Autowired
    private AssignmentRepository assignmentRepository;

    public Optional<Assignment> getAssignment(final Long id) {
        return assignmentRepository.findById(id);
    }

    public Iterable<Assignment> getAssignments() {
        return assignmentRepository.findAll();
    }

    public void deleteAssignment(final Long id) {
        assignmentRepository.deleteById(id);
    }

    public Assignment saveAssignment(Assignment assignment) {
        Assignment savedAssignment = assignmentRepository.save(assignment);
        return savedAssignment;
    }

    public Optional<Assignment> findById(long id) {
        return assignmentRepository.findById(id);
    }
}
