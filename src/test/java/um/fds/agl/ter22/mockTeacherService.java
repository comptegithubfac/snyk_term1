package um.fds.agl.ter22;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class mockTeacherService {
    @Autowired
    private MockMvc mvc;
    @MockBean
    TeacherService teacherService;

    @Captor
    ArgumentCaptor<Teacher> captor;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                        .param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        ArgumentCaptor<Teacher> captor = ArgumentCaptor.forClass(Teacher.class);
        verify(teacherService, times(1)).saveTeacher(captor.capture());

        Teacher actual = captor.getValue();
        assertEquals("Anne-Marie", actual.getFirstName());
    }


    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void updateTeacherFromManager() throws Exception {
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Ado")
                        .param("lastName", "ken")
                        .param("id", "2")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        ArgumentCaptor<Teacher> captor = ArgumentCaptor.forClass(Teacher.class);
        verify(teacherService, times(1)).saveTeacher(captor.capture());

        Teacher actual = captor.getValue();
        assertEquals("Ado", actual.getFirstName());
    }

}
