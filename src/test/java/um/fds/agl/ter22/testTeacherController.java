package um.fds.agl.ter22;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import um.fds.agl.ter22.services.TeacherService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class testTeacherController {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private TeacherService teacherService;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                                .andExpect(view().name("addTeacher"))
                                .andReturn();
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void testLoginManager() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assertTrue(teacherService.getTeacher(10l).isEmpty());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                        .param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertTrue(teacherService.getTeacher(10l).isEmpty());
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherAssumingNonExistingTeacher() throws Exception {
        assumingThat((teacherService.getTeacher(10l).isEmpty()), () -> {
            MvcResult result = mvc.perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            .param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });

        assertTrue(teacherService.getTeacher(10l).isEmpty());
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void updateTeacherFromManager() throws Exception {
        assertTrue(teacherService.getTeacher(2l).isPresent());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("firstName", "Ado")
                        .param("lastName", "ken")
                        .param("id", "2")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertEquals("Optional[Teacher{id=2, firstName='Ado', lastName='ken', manager='Manager{id=1, name='Chef', roles=[ROLE_MANAGER]}'}]", (teacherService.getTeacher(2l)).toString());
    }

}
