package um.fds.agl.ter22;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.repositories.TERManagerRepository;
import um.fds.agl.ter22.repositories.TeacherRepository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class testTeacherRepository {

    @Autowired
    private TeacherRepository teachers;
    @Autowired
    private TERManagerRepository managers;

    @Test
    void saveIsPossibleForManager() {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("lechef", "peu importe", AuthorityUtils.createAuthorityList("ROLE_MANAGER")));
        TERManager terM1Manager = new TERManager("Mathieu", "lechef", "mdp", "ROLE_MANAGER");

        this.managers.save(terM1Manager);
        this.teachers.save(new Teacher("Margaret", "Hamilton", "margaret", terM1Manager, "ROLE_TEACHER"));
        assertThat(teachers.findByLastName("Hamilton"), is(notNullValue()));
    }

    @Test
    void saveIsPossibleForTeacher() {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Lovelace", "lovelace", AuthorityUtils.createAuthorityList("ROLE_TEACHER")));

        TERManager terM1Manager = new TERManager("Ada", "Lovelace", "lovelace", "ROLE_TEACHER");

        this.managers.save(terM1Manager);


        assertThrows(org.springframework.security.access.AccessDeniedException.class, () -> {
            this.teachers.save(new Teacher("Among", "Us", "margaret", terM1Manager, "ROLE_TEACHER"));
        });
    }



}
